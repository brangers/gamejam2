import "phaser"
import {TileCard} from "./tilesprite.js"
import { TextButton } from "./textButton.js";

var moment = require('moment');

export class Game extends Phaser.Scene
{
    constructor() {
        super({key: 'game'});
    }

    init(){
        this.i = 0;
        this.id = [];
        this.score = 0;
        this.cards = 0;
        var element = document.createElement('style');

        document.head.appendChild(element);

        var sheet = element.sheet;

        var styles = '@font-face { font-family: "troika"; src: url("/assets/font/Bombing.ttf") format("opentype"); }\n';

        sheet.insertRule(styles, 0);
        this.datenow = moment().unix();
    }

    getRandomInt(max) { // Tire un nombre aléatoire entre 0 et max.
        return Math.floor(Math.random() * Math.floor(max));
    }


    chooseCardsAuto(frames, count) { // Choisi des cartes dans le packet.
        var cardPack = [];
        var j = 0;
        console.log(frames)
        for (var i = 0; i < count;) {
            var card = frames[this.getRandomInt(count)];
            if (!cardPack.find(element => element == card )) {
                cardPack.push(card);
                i += 1
            }
            // card.forEach()
            // }
        }
        return (cardPack);
    }

    loopmusic(first)
    {
        first.play();
        first.setLoop(true);
    }

    doubleCards(pack, count, frames) { // Double les cartes.
        var cardPack = pack;
        var id = []
        console.log(frames)
        for (var i = 0; i < count;) {
            var card = frames[this.getRandomInt(count)];
            if (!id.find(element => element == card )) {
                cardPack.push(card);
                id.push(card);
                i += 1
            }
            // card.forEach()
            // }
        }
        return (cardPack);
    }

    preload()
    {
        this.load.image("backgroundGame", "/assets/img/wall.jpg");
        this.load.script('webfont', '/assets/js/webfont.js');
        this.load.atlas('cards', '/assets/cards/london/cards.png', 'assets/cards/london/frames.json');
        this.load.audio('music', '/assets/rap_beat_classic_90s.mp3')
    }

    createText(text) {
    }
    create()
    {
        this.background = this.add.sprite(0, 0, "backgroundGame");
        var graphics = this.add.graphics();
        graphics.fillStyle(0x000000, 1);
        graphics.fillRect(950, 50, 256, 512);
        let vm = this;
        var first = this.sound.add('music', { loop: true });
        this.loopmusic(first);
        WebFont.load({
            custom: {
                families: [ 'troika']
            },
            active: function ()
            {
                let Score = vm.add.text(1280 - 200, 50, 'SCORE', { fontFamily: 'troika', fontSize: 80, color: '#ffffff' })
                    .setOrigin(0.5, 0)
                vm.ScoreNb = new TextButton(vm, 1280 - 200, 150, '0', { fontFamily: 'troika', fontSize: 40, color: '#ffffff' }, 10)
                    .setOrigin(0.5, 0)
                vm.timeNb = new TextButton(vm, 1280 - 200, 350, '', { fontFamily: 'troika', fontSize: 40, color: '#ffffff' }, 10)
                    .setOrigin(0.5, 0)
                vm.add.existing(vm.ScoreNb)
                vm.add.existing(vm.timeNb)
            }
        });
        var back = ['back'];
        var frames = [];
        for (let a = 1; a < 16; a += 1) {
            frames.push("london" + a);
        }
        this.tilesprites = [];
        var deck = this.chooseCardsAuto(frames, 15); 
        deck = this.doubleCards(deck, 15, frames);
        var x = 100;
        var y = 100;
        var z = 0;
        let carte;
        for (var i = 0; i < 30; i++) {
            carte = deck[i];
            if (z > 5) {
                y += 120;
                x = 100;
                z = 0;
            }
            this.tilesprites[i] = new TileCard(this, x, y, 140, 190, 'cards', 'back', carte)
                .setName(carte)
            this.add.existing(this.tilesprites[i]);
            x += 137;
            z++;
        
            // this.input.on('dragstart', function (pointer, gameObject) {
        //     this.children.bringToTop(gameObject);
        // }, this);
        }
    }
        //do stuff
    update() {
        this.timeEnd = moment().unix() - this.datenow;
        if (this.i >= 30)
            this.i = 0;
        if (this.tilesprites[this.i].open == true && this.id.length < 3 && this.id[0] != this.i && this.id[1] != this.i) {
            console.log("open")
            this.id.push(this.i);
        } if (this.id.length == 2) {
            for (var z = 0; z < 30; z += 1)
                this.tilesprites[z].block();
            if (this.tilesprites[this.id[0]].name == this.tilesprites[this.id[1]].name) {
                if (this.id[0] != undefined && this.id[1] != undefined) {
                    this.tilesprites[this.id[0]].open = false
                    this.tilesprites[this.id[1]].open = false
                    this.tilesprites[this.id[0]].delete(this.tilesprites)
                    this.tilesprites[this.id[1]].delete(this.tilesprites)
                    this.id.length = 0;
                    this.score += 100;
                    this.cards += 2;
                    this.ScoreNb.changeText(this.score.toString());
                    if (this.cards >= 30) {
                        this.timeNb.win(this.timeEnd)
                    }
                }
            }
            else {
                if (this.id[0] != undefined && this.id[1] != undefined) {
                    this.tilesprites[this.id[0]].open = false
                    this.tilesprites[this.id[1]].open = false
                    this.tilesprites[this.id[0]].reset(this.tilesprites)
                    this.tilesprites[this.id[1]].reset(this.tilesprites)
                    this.score -= 10;
                    if (this.score < 0)
                        this.score = 0;
                    this.ScoreNb.changeText(this.score.toString());
                    this.id.length = 0;
                }
            }
        }
        this.i += 1;
    //     for (var i = 0; i < 15; i += 1) {
    //         // console.log(this.id.length)
    //         if (this.tilesprites[i].open == true && this.id.length < 3) {
    //             // console.log("Je suis ouvert")
    //             this.id.push(i)
                
    //         } if (this.id.length == 2) {
    //             if (this.tilesprites[this.id[0]].name === this.tilesprites[this.id[1]].name) {
    //                 if (this.id[0] && this.id[1]) {
    //                     this.tilesprites[this.id[0]].open = false
    //                     this.tilesprites[this.id[1]].open = false
    //                     this.tilesprites[this.id[0]].delete()
    //                     this.tilesprites[this.id[1]].delete()
    //                     this.id.length = 0;
    //                 }
    //             }
    //             else {
    //                 if (this.id[0] && this.id[1]) {
    //                     this.tilesprites[this.id[0]].open = false
    //                     this.tilesprites[this.id[1]].open = false
    //                     this.tilesprites[this.id[0]].reset()
    //                     this.tilesprites[this.id[1]].reset()
    //                     this.id.length = 0;
    //                 }
    //             }
    //             break;
    //         }
    //     }
    }
}
function formatTime(seconds){
    // Minutes
    var minutes = Math.floor(seconds/60);
    // Seconds
    var partInSeconds = seconds%60;
    // Adds left zeros to seconds
    partInSeconds = partInSeconds.toString().padStart(2,'0');
    // Returns formated time
    return `${minutes}:${partInSeconds}`;
}