import "phaser"
import { TextButton } from "./textButton"
class sceneMenu extends Phaser.Scene {
    constructor() {
        super({key: 'sceneMenu'});
    }
    
    init() {
        //  Inject our CSS
        var element = document.createElement('style');

        document.head.appendChild(element);

        var sheet = element.sheet;

        var styles = '@font-face { font-family: "troika"; src: url("/assets/font/Bombing.ttf") format("opentype"); }\n';

        sheet.insertRule(styles, 0);
    }

    preload() {
        this.load.script('webfont', '/assets/js/webfont.js');
        this.load.image("background", "/assets/img/background.jpg");
    }

    create() {
        var add = this.add;
        let vm = this;
        var input = this.input;
        var playButton;

        this.background = this.add.sprite(0, 0, "background")
            .setScale(0.65)
            .setOrigin(0, 0)
        WebFont.load({
            custom: {
                families: [ 'troika']
            },
            active: function ()
            {
                playButton = new TextButton(vm, 1280/2, 350, 'PLAY GAME', { fontFamily: 'troika', fontSize: 80, color: '#ffffff' })
                    .setOrigin(0.5, 0)
                    .on('pointerover', () => {
                        playButton.on('pointerdown', () => {
                            vm.background.destroy();
                            vm.scene.start("game")
                        })
                    })
                add.existing(playButton);
            }
        });
        
    }

    update() {
    }
}
export default sceneMenu;