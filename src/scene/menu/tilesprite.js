import "phaser"

export class TileCard extends Phaser.GameObjects.TileSprite {
    constructor(scene, x, y, width, height, textureKey, frameKey, frameKey1) {
        super(scene, x, y, width, height, textureKey, frameKey, frameKey1);
        this.setScale(0.6, 0.6)
        this.open = false;
        this.notopen = false;
        this.blockd = true;
        this.setFrame(frameKey1);
        setTimeout(() => {
          this.setFrame("back");
          this.blockd = false;
        }, 2000)
        this.setInteractive({ useHandCursor: true })
          .on('pointerover', () => {
            this.on('pointerdown', () => {
            if (this.blockd == false) {
              this.setFrame(frameKey1);
              this.name = frameKey1;
              this.open = true;
            }
          })
        })
      }
      block() {
        this.blockd = true;
      }
      unblock() {
        this.blockd = false;
      }
      reset(tilesprites) {
        this.tilesprites = tilesprites
        setTimeout(() => {
          this.setFrame("back");
          for (var z = 0; z < 30; z += 1)
                this.tilesprites[z].blockd = false;
        }, 500)
      }
      delete(tilesprites) {
        this.tilesprites = tilesprites
        setTimeout(() => {
          this.destroy();
          for (var z = 0; z < 30; z += 1)
              this.tilesprites[z].blockd = false;
        }, 500)
      }
      test() {
        console.log("Salut");
      }

}