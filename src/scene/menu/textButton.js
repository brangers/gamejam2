import "phaser"

export class TextButton extends Phaser.GameObjects.Text {
    constructor(scene, x, y, text, style, id) {
      super(scene, x, y, text, style, id);
      this.initialTime = 0;
      this.setInteractive({ useHandCursor: true })
        .on('pointerover', () => {
            if (id == 10) {
            } else
                this.OverButton(1)
        })
        .on('pointerout', () => {
            if (id == 10) {
            } else
                this.OverButton(0);
        });
    }
    win(time) {
        this.setText("You win in :\n" + time + " s") 
    }
    changeTime(time) {
        this.setText(time + " s")
    }
    changeText(score) {
        this.setText(score)
    }
    OverButton(i) {
        if (i === 0) {
            this.setStyle({ fill: '#ffffff'});
        } else if (i == 1) {
            this.setStyle({ fill: '#FFFA97'});
        }
    }

}

function formatTime(seconds){
    // Minutes
    var minutes = Math.floor(seconds/60);
    // Seconds
    var partInSeconds = seconds%60;
    // Adds left zeros to seconds
    partInSeconds = partInSeconds.toString().padStart(2,'0');
    // Returns formated time
    return `${minutes}:${partInSeconds}`;
}