import 'phaser';
import sceneMenu from './scene/menu/sceneMenu.js'
import { Game } from './scene/menu/game.js'

let titleMenu = new sceneMenu(); // creation de la scene du men
let gameScene = new Game();

let config = {
    type: Phaser.AUTO,
    width: 1280,
    height: 720,
    backgroundColor: 0x000000,
    scene : [titleMenu, gameScene],
    disableWebAudio: true,
  };

let game = new Phaser.Game(config);